package libs;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class ActionsWithWebElements {
    WebDriver driver;
    Logger log;

    public ActionsWithWebElements (WebDriver webDriver) {
        this.driver = webDriver;
        log= Logger.getLogger(getClass());
    }
    public boolean isElementPresent (WebElement element) {
            boolean value=false;
        return (element.isDisplayed() && element.isEnabled());
    }
    public void inputTextToField(WebElement element, String inputText){
        try {
            element.clear();
            element.sendKeys(inputText);
//            log.info("Text has been entered");

        } catch (Exception ex) {
            log.error("Text wasn't entered");
            ex.printStackTrace();
            Assert.fail("Malfunctioned input");
        }
    }
    public void pushTheButton(WebElement element) {
        try {
            element.click();
//            log.info("Button has been pushed");
        } catch (Exception ex) {
            log.error("Button isn't pushed");
            ex.printStackTrace();
            Assert.fail("Malfunctioned button");
        }
    }
    public void ddSelectbyText (String ddLocator, String ddText) {
        try {
            Select selectDD = new Select(driver.findElement(By.xpath(ddLocator)));
            selectDD.selectByVisibleText(ddText);
            log.info("Dropdown works ok");
        } catch (Exception ex) {
            log.error("Element isn't selected");
            ex.printStackTrace();
            Assert.fail("Element isn't selected");
        }
    }
    public void ddSelectbyValue (String ddLocator, String ddValue){
        try {
            Select selectDD = new Select(driver.findElement(By.xpath(ddLocator)));
            selectDD.selectByValue(ddValue);
            log.info("Dropdown works ok");
        } catch (Exception ex) {
            log.error("Element isn't selected");
            ex.printStackTrace();
            Assert.fail("Element isn't selected");
        }
    }

    /**
     * проверка чекбоксов и радиобаттонов
     */
    public void selectCheckBox (String checkboxLocator, boolean neededStatus) {
        try {
            if (neededStatus && (!driver.findElement(By.xpath(checkboxLocator)).isSelected())||(!neededStatus && (driver.findElement(By.xpath(checkboxLocator)).isSelected()))) {
                driver.findElement(By.xpath(checkboxLocator)).click();
            }
//            log.info("Element is checked");
        } catch (Exception ex) {
            log.error("Element isn't checked");
            ex.printStackTrace();
            Assert.fail("Element isn't checked");
        }
    }
}

