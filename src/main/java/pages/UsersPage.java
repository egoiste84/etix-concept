package pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class UsersPage extends ParentPage {


    @FindBy(xpath = "//a[.='Admin Panel']\n")
    private WebElement AdminPanelButton;
    @FindBy (xpath = "//span[@class='Button css-18fb5p7']\n")
    private WebElement createUserButton;
    @FindBy(xpath ="//input[@id='firstName']")
    private WebElement firstNameField;
    @FindBy(xpath = "//input[@id='lastName']")
    private WebElement lastNameField;
    @FindBy(xpath="//input[@id='mobilePhone']")
    private WebElement phoneNumberField;
    @FindBy(xpath = "//i[@name='drop-arrow-bottom']\n")
    private WebElement organizationDD;
    @FindBy(xpath = "//*[@id=\"react-select-2--option-0\"]")
    private WebElement organizationSelector;
    @FindBy(xpath = "//input[@class='css-1rqg7si']")
    private WebElement searchField;
    @FindBy(xpath = "//i[@name='more']")
    private WebElement moreButton;
    @FindBy(xpath = "//span[.='Resend confirmation email']\n")
    private WebElement resendConfirmMail;
    @FindBy(xpath = "//span[.='Delete User']\n")
    private WebElement deleteUser;
    @FindBy(xpath = "//button[@class='Modal-submit css-1h5u9sk']//span[@class='Button css-18fb5p7']\n")
    private WebElement deleteConfirmation;
//    String TitleLogo = "//a[@class='logo']";

    public UsersPage(WebDriver driver)
    {
        super(driver);
    }

//    public boolean isAvatarPresent() {
//        return actionsWithWebElements.isElementPresent(avatar);
//    }

//    public void compareUrl() {
//        actionsWithWebElements.checkUrl("http://v3.test.itpmgroup.com/");
//    }
    public void goToAdminPanel() {actionsWithWebElements.pushTheButton(AdminPanelButton); }
    public void setCreateUserButton() {actionsWithWebElements.pushTheButton(createUserButton);}
    public void setFirstNameField(String name) { actionsWithWebElements.inputTextToField(firstNameField, name); }
    public void setLastNameField(String surname) {actionsWithWebElements.inputTextToField(lastNameField, surname);}
    public void setPhoneNumberField(String number) {actionsWithWebElements.inputTextToField(phoneNumberField, number);}
    public void setOrganizationDD() {actionsWithWebElements.pushTheButton(organizationDD);}
    public void setOrganizationSelector() {actionsWithWebElements.pushTheButton(organizationSelector);}
    public void setSearchField(String mail) {actionsWithWebElements.inputTextToField(searchField, mail);}
    public void setMoreButton() {actionsWithWebElements.pushTheButton(moreButton);}
    public void setResendConfirmMail() {actionsWithWebElements.pushTheButton(resendConfirmMail);}
    public void setDeleteUser() {actionsWithWebElements.pushTheButton(deleteUser);}
    public void setDeleteConfirmation() {actionsWithWebElements.pushTheButton(deleteConfirmation);}
}
