package pages;

//import org.junit.Assert;
//import junit.framework.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
        import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;

public class AssetsPage extends ParentPage {
    WebDriverWait wait_url = new WebDriverWait(driver, 50);
    @FindBy(xpath = "//nav[2]/ul[@class='MuiList-root MuiList-padding']/div[.='ASSETS']\n")
    WebElement assetSelector;
    @FindBy(xpath = "//a[@href='/assets/racks']\n")
    WebElement racksSwitcher;

    @FindBy(css = ".css-19bqh2r\n")
    WebElement dataCenterDD;
    @FindBy(xpath = "//*[@id=\"react-select-2-option-0-0\"]")
    WebElement dataCenterSelector;
    @FindBy(xpath = "//*[@id=\"root\"]/div[1]/div[2]/div/div[2]/div[2]/div/table/tbody/tr[1]/td[1]/a")
    WebElement rackSelector;
    @FindBy(xpath="//span[.='Create Booking']\n")
    WebElement addBooking;
    @FindBy(xpath = "//div[@class='Select css-5ojq3w is-searchable Select--single']//i[@name='drop-arrow-bottom']\n")
    WebElement selectDD;
    @FindBy(css="#react-select-3--option-0")
    WebElement orientationSelect;
    @FindBy(xpath="//div[@class='Select css-5ojq3w is-clearable is-searchable Select--multi']//i[@name='drop-arrow-bottom']\n")
    WebElement positionDD;
    @FindBy(xpath="//*[@id=\"react-select-4--option-0\"]")
    WebElement positionSelect;
    @FindBy(css = "form.css-0 > div:nth-of-type(5) #date\n")
    WebElement endDatePicker;
    @FindBy(xpath = "//div[@class='CalendarMonthGrid CalendarMonthGrid--horizontal']//tr[4]//div[.='28']\n")
    WebElement endDateSelector;
    @FindBy(css = "[name='comment']\n")
    WebElement commentField;
    @FindBy(xpath = "/html/body/div[4]/div/div/div/div[2]/form/div[7]/button[2]/div/span")
    WebElement submitBookingButton;
	@FindBy(xpath= "//a[.='ASSETS']\n")
    WebElement assetSwitcher;
	@FindBy(xpath="//span[.='Create']\n")
	WebElement createAssetButton;
	@FindBy(xpath="//div[@class='css-1v5rz78']/div[1]//span[@class='Select-arrow-zone']\n")
	WebElement rackDD;
    @FindBy(xpath = "//*[@id=\"react-select-2--option-0\"]")
    WebElement rackSelector2;
    @FindBy(xpath = "//div[@class='css-1v5rz78']/div[3]//span[@class='Select-arrow-zone']\n")
    WebElement assetModelDD;
    @FindBy(xpath = "//*[@id=\"react-select-4--option-5\"]")
    WebElement assetModelSelector;
    @FindBy(xpath = "//*[@id=\"root\"]/div[1]/div[2]/form/div/div[4]/div[2]/div/div/div/span[2]")
    WebElement assetPositionDD;
    @FindBy(xpath = "//*[@id=\"react-select-6--option-4\"]")
    WebElement assetPositionSelector;
    @FindBy(xpath = "//div[@class='css-1v5rz78']/div[5]//span[@class='Select-arrow-zone']\n")
    WebElement assetDirectionDD;
    @FindBy(xpath = "//*[@id=\"react-select-7--option-0\"]")
    WebElement assetDirectionSelector;
    @FindBy (xpath = "//input[@id='name']\n")
    WebElement nameField;
    @FindBy (xpath = "//input[@id='reference']\n")
    WebElement referenceField;
    @FindBy(xpath = "//span[.='Save']\n")
    WebElement saveButton;
    @FindBy(xpath = "//a[.='CABLES']\n")
    WebElement cableSelector;
    @FindBy (xpath = "//*[@id=\"root\"]/div[1]/div[2]/header/div/div[2]/div[2]/a/span")
    WebElement createCableButton;
    @FindBy (xpath = "//input[@id='name']\n")
    WebElement cableNameField;
    @FindBy (xpath = "//input[@id='reference']\n")
    WebElement cableReferenceField;
    @FindBy (xpath = "//div[@class='css-1v5rz78']/div[1]//span[@class='Select-arrow-zone']\n")
    WebElement cableTypeDD;
    @FindBy (xpath = "//*[@id=\"react-select-2--option-2\"]")
    WebElement cableTypeSelector;
    @FindBy (xpath = "//div[@class='css-1v5rz78']/div[3]//span[@class='Select-arrow-zone']\n")
    WebElement sourceAssetTypeDD;
    @FindBy (xpath = "//*[@id=\"react-select-3--option-1\"]")
    WebElement sourceAssetTypeSelector;
    @FindBy (xpath = "//div[@class='css-1v5rz78']/div[4]//span[@class='Select-arrow-zone']\n")
    WebElement sourceAssetDD;
    @FindBy (xpath = "//*[@id=\"react-select-4--option-1\"]")
    WebElement sourceAssetSelector;
    @FindBy (xpath = "//div[@class='css-1v5rz78']/div[5]//span[@class='Select-arrow-zone']\n")
    WebElement sourceOutletDD;
    @FindBy (xpath = "//*[@id=\"react-select-5--option-0\"]")
    WebElement sourceOutletSelector;
    @FindBy (xpath = "//div[@class='css-1v5rz78']/div[@class='css-1y341wq']//div[@class='Select css-5ojq3w is-searchable Select--single']//span[@class='Select-arrow-zone']\n")
    WebElement destinationAssetTypeDD;
    @FindBy (xpath = "//*[@id=\"react-select-6--option-1\"]")
    WebElement destinationAssetTypeSelector;
    @FindBy (xpath = "//div[@class='css-1v5rz78']/div[8]//span[@class='Select-arrow-zone']\n")
    WebElement destinationAssetDD;
    @FindBy (xpath = "//*[@id=\"react-select-7--option-0\"]")
    WebElement destinationAssetSelector;
    @FindBy (xpath = "//div[@class='css-1v5rz78']/div[9]//span[@class='Select-arrow-zone']\n")
    WebElement detinationOutletDD;
    @FindBy (xpath = "//*[@id=\"react-select-8--option-0\"]")
    WebElement destinationOutletSelector;
    @FindBy (xpath = "//span[.='Save']\n")
    WebElement cableCreateButton;
    @FindBy (xpath = "//a[.='CABLE CHAINS']\n")
    WebElement cableChainSwitcher;
    @FindBy (xpath = "//a[.='Create']\n")
    WebElement newCableChainButton;
    @FindBy (xpath = "//input[@name='name']\n")
    WebElement cableChainName;
    @FindBy (css = "div.css-zldx2o > div:nth-of-type(1) div:nth-of-type(2) > div:nth-of-type(1) > div:nth-of-type(1) svg:nth-of-type(1)\n")
    WebElement rackCompartmentDD;
    @FindBy (xpath = "//*[@id=\"react-select-4-option-0\"]")
    WebElement rackCompartmentSelector;
    @FindBy (css = "div.css-zldx2o div:nth-of-type(2) div:nth-of-type(3) svg:nth-of-type(1)\n")
    WebElement assetSelectorDD;
    @FindBy (xpath = "//*[@id=\"react-select-5-option-2\"]")
    WebElement assetSelector2;
    @FindBy (css = "div.css-zldx2o div:nth-of-type(2) div:nth-of-type(4) svg:nth-of-type(1)\n")
    WebElement outOutletDD;
    @FindBy (xpath = "//*[@id=\"react-select-6-option-1\"]")
    WebElement outOutletSelector;
    @FindBy (css = "div.css-zldx2o > div:nth-of-type(1) > div:nth-of-type(1) > div:nth-of-type(3) > div:nth-of-type(1) > div:nth-of-type(1) svg:nth-of-type(1)\n")
    WebElement inRackCompartmentDD;
    @FindBy (xpath = "//*[@id=\"react-select-7-option-0\"]")
    WebElement inRackCompartmentSelector;
    @FindBy (css = "div.css-zldx2o div:nth-of-type(3) div:nth-of-type(3) svg:nth-of-type(1)\n")
    WebElement inAssetPduDD;
    @FindBy (xpath = "//*[@id=\"react-select-8-option-1\"]")
    WebElement inAssetPduSelector;
    @FindBy (css = "div.css-zldx2o div:nth-of-type(3) div:nth-of-type(4) svg:nth-of-type(1)\n")
    WebElement inOutletSelectDD;
    @FindBy (xpath = "//*[@id=\"react-select-9-option-1\"]")
    WebElement inOutletSelector;
    @FindBy (xpath = "//button[@class='css-17cue0v']\n")
    WebElement createCable2Button;
    @FindBy (xpath = "//*[@id=\"name\"]")
    WebElement cableNameInput;
    @FindBy (xpath = "/html/body/div[4]/div/div/div/div[2]/form/div[2]/button[1]/div/span")
    WebElement cableSaveButton;
    @FindBy (xpath = "//*[@id=\"root\"]/div[1]/div[2]/article/form/header/div/div/div/button/span")
    WebElement cableChainSaveButton;
    public AssetsPage(WebDriver driver) {super(driver);}
    public void selectAsset() {
        actionsWithWebElements.pushTheButton(assetSelector);
        driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
        log.info("Redirected to Assets");
    }
    public void setRacksSwitcher() {actionsWithWebElements.pushTheButton(racksSwitcher);}
    public void dataCenterDD() {
        actionsWithWebElements.pushTheButton(dataCenterDD);
    }
    public void selectDataCenter() { actionsWithWebElements.pushTheButton(dataCenterSelector);
        log.info("Data Center selected");
    }
    public void selectRack() {
        actionsWithWebElements.pushTheButton(rackSelector);
        driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
        log.info("Rack selected");
    }
    public void addBook(){
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0,1400);");
        actionsWithWebElements.pushTheButton(addBooking);}
    public void selectDD() {actionsWithWebElements.pushTheButton(selectDD);}
    public void orientationSelect() {actionsWithWebElements.pushTheButton(orientationSelect);}
    public void positioningDD() {actionsWithWebElements.pushTheButton(positionDD);}
    public void positioningSelect(By xpath) {actionsWithWebElements.pushTheButton(positionSelect);}
    public void endDatePickerOpen() {actionsWithWebElements.pushTheButton(endDatePicker);}
    public void endDateSelect() {actionsWithWebElements.pushTheButton(endDateSelector);}
    public void commentSet(String comment) {actionsWithWebElements.inputTextToField(commentField, comment);}
    public void submitBooking() throws InterruptedException {actionsWithWebElements.pushTheButton(submitBookingButton);
        Thread.sleep(2000);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0,0);");
        Thread.sleep(2000);
        log.info("Rack space successfully booked");
    }
    public void switchToAssets() {actionsWithWebElements.pushTheButton(assetSwitcher);
        log.info("Switched to Assets");
    }
    public void createNew() {actionsWithWebElements.pushTheButton(createAssetButton);}
    public void rackDDSelection() {actionsWithWebElements.pushTheButton(rackDD);}
    public void rackSelection() {actionsWithWebElements.pushTheButton(rackSelector2);}
    public void assetModelDDSelection() {actionsWithWebElements.pushTheButton(assetModelDD);}
    public void assetModelSelection() {actionsWithWebElements.pushTheButton(assetModelSelector);}
    public void assetPositionDDOpen() {actionsWithWebElements.pushTheButton(assetPositionDD);}
    public void assetPositionSelect() {actionsWithWebElements.pushTheButton(assetPositionSelector);}
    public void assetDirectionDDOpen() {actionsWithWebElements.pushTheButton(assetDirectionDD);}
    public void assetDirectionSelect() {actionsWithWebElements.pushTheButton(assetDirectionSelector);}
    public void fillNameField(String name) {actionsWithWebElements.inputTextToField(nameField, name);}
    public void fillReferenceField(String reference) {actionsWithWebElements.inputTextToField(referenceField, reference);}
    public void setRadioInstalled() {actionsWithWebElements.selectCheckBox("//*[@id=\"status-INSTALLED\"]", true);}
    public void setPowerOutlet2() {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(500,1300);");
        actionsWithWebElements.selectCheckBox("//div[@class='css-1v5rz78']//div[1]/div[3]//input[@value='OUT']\n", true);
    }
    public void setNetworkOutlet1() {actionsWithWebElements.selectCheckBox("//div[@class='css-1v5rz78']//div[2]/div[2]//input[@value='IN']\n", true);}
    public void setNetworkOutlet3() {actionsWithWebElements.selectCheckBox("//div[@class='css-1v5rz78']/div[11]/div[2]/div[4]//input[@value='IN']\n", true);}
    public void setConsoleOutlet2() {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(1300, 15000);");
        actionsWithWebElements.selectCheckBox("//div[@class='css-1v5rz78']//div[3]/div[3]//input[@value='OUT']\n", true);
    }
    public void setConsoleOutlet4() {actionsWithWebElements.selectCheckBox("//div[@class='css-1v5rz78']//div[3]/div[5]//input[@value='OUT']\n", true);}
    public void assetSave() throws InterruptedException {
        actionsWithWebElements.pushTheButton(saveButton);
        log.info("Asset successfully created");
        Thread.sleep(2000);
    }
    public void switchToCables() {
        actionsWithWebElements.pushTheButton(cableSelector);
        log.info("Switched to Cables");
    }
    public void createNewCable() {actionsWithWebElements.pushTheButton(createCableButton);}
    public void fillCableName(String name) {actionsWithWebElements.inputTextToField(cableNameField, name);}
    public void fillCableReference(String reference) {actionsWithWebElements.inputTextToField(cableReferenceField, reference);}
    public void cableTypeDDOpen() {actionsWithWebElements.pushTheButton(cableTypeDD);}
    public void cableTypeSelect() {actionsWithWebElements.pushTheButton(cableTypeSelector);}
    public void sourceAssetTypeDDOpen() {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(500,1300);");
        actionsWithWebElements.pushTheButton(sourceAssetTypeDD);}
    public void sourceAssetTypeSelect() {actionsWithWebElements.pushTheButton(sourceAssetTypeSelector);}
    public void sourceAssetDDOpen() {actionsWithWebElements.pushTheButton(sourceAssetDD);}
    public void sourceAssetSelect() {actionsWithWebElements.pushTheButton(sourceAssetSelector);}
    public void sourceOutletDDOpen() {actionsWithWebElements.pushTheButton(sourceOutletDD);}
    public void sourceOutletSelect() {actionsWithWebElements.pushTheButton(sourceOutletSelector);}
    public void destinationAssetTypeDDOpen() {actionsWithWebElements.pushTheButton(destinationAssetTypeDD);}
    public void setDestinationAssetTypeSelect() {actionsWithWebElements.pushTheButton(destinationAssetTypeSelector);}
    public void destinationAssetDDOpen() {actionsWithWebElements.pushTheButton(destinationAssetDD);}
    public void destinationAssetSelect() {actionsWithWebElements.pushTheButton(destinationAssetSelector);}
    public void destinationOutletDDOpen() {actionsWithWebElements.pushTheButton(detinationOutletDD);}
    public void destinationOutletSelect() {actionsWithWebElements.pushTheButton(destinationOutletSelector);}
    public void createCable() throws InterruptedException {actionsWithWebElements.pushTheButton(cableCreateButton);
        log.info("Cable successfully created");
        Thread.sleep(2000);
    }
    public void switchToCableChain() {actionsWithWebElements.pushTheButton(cableChainSwitcher);
    log.info("Switched to Cable Chains"); }
    public void createCableChain() {actionsWithWebElements.pushTheButton(newCableChainButton);}
    public void fillCableChainName(String name) {actionsWithWebElements.inputTextToField(cableChainName, name);}
    public void rackCompartmentDDOpen() {actionsWithWebElements.pushTheButton(rackCompartmentDD);}
    public void rackCompartmentSelect() {actionsWithWebElements.pushTheButton(rackCompartmentSelector);}
    public void setAsset() {actionsWithWebElements.selectCheckBox("//div[@class='css-zldx2o']//div[@class='css-1v5rz78']/div[2]//input[@value='asset']\n", true);}
    public void assetSelectorDDOpen() {actionsWithWebElements.pushTheButton(assetSelectorDD);}
    public void assetSelect2() {actionsWithWebElements.pushTheButton(assetSelector2);}
    public void outOutletDDOpen() {actionsWithWebElements.pushTheButton(outOutletDD);}
    public void outOutletSelect() {actionsWithWebElements.pushTheButton(outOutletSelector);}
    public void inRackCompartmentDDOpen() {actionsWithWebElements.pushTheButton(inRackCompartmentDD);}
    public void inRackCompartmentSelect() {actionsWithWebElements.pushTheButton(inRackCompartmentSelector);}
    public void setInAsset() {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0,1300);");
        actionsWithWebElements.selectCheckBox("//div[@class='css-zldx2o']//div[3]//input[@value='asset']\n", true);}
    public void inAssetPduDDOpen() {actionsWithWebElements.pushTheButton(inAssetPduDD);}
    public void inAssetPduSelect() {actionsWithWebElements.pushTheButton(inAssetPduSelector);}
    public void inOutletSelectDDOpen() {actionsWithWebElements.pushTheButton(inOutletSelectDD);}
    public void inOutletSelect() {actionsWithWebElements.pushTheButton(inOutletSelector);}
    public void createCable2() {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0,0);");
        actionsWithWebElements.pushTheButton(createCable2Button);}
    public void fillCableNameInput(String name) {actionsWithWebElements.inputTextToField(cableNameInput, name); }
    public void setCableType() {actionsWithWebElements.selectCheckBox("//*[@id=\"subType-OTHER\"]", true); }
    public void setCableSaveButton() throws InterruptedException {actionsWithWebElements.pushTheButton(cableSaveButton);
        Thread.sleep(2000);
    }
    public void setCableChainSaveButton() throws InterruptedException {
        actionsWithWebElements.pushTheButton(cableChainSaveButton);
        log.info("Cable chain successfully created");
        Thread.sleep(2000);
    }
}


