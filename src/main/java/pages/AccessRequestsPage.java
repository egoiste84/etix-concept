package pages;

import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.concurrent.TimeUnit;

public class AccessRequestsPage extends ParentPage {
    private WebDriverWait wait_url = new WebDriverWait(driver, 10);

    public AccessRequestsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//h6[.='ACCESS']\n")
    WebElement accessDD;
    @FindBy(xpath = "//h6[.='ACCESS REQUESTS']\n")
    WebElement accessRequest;
    @FindBy(xpath = "//*[@id=\"content\"]/ui-view/div/div[1]/div[1]/a")
    WebElement newRequest;
    @FindBy(xpath = "//input[@id='generalSubject']\n")
    WebElement subjectInput;
    @FindBy (xpath = "//select[@id='generalDatacenter']\n")
    WebElement dataCenterDD;
    @FindBy (xpath = "//select[@id='generalOrganizations']\n")
    WebElement organizationSelector;
    @FindBy (xpath = "//select[@id='generalType']\n")
    WebElement typeSelector;
    @FindBy (xpath = "//*[@id=\"generalType\"]")
    WebElement typeSelectorQA;
    @FindBy (xpath = "//button[@class='btn btn-success btn-block']\n")
    WebElement endStepOneButton;
    @FindBy (xpath = "//*[@id=\"ar-page\"]/div[3]/div[2]/fieldset[1]/div[2]/div/div/input")
    WebElement registeredUserDD;
    @FindBy (xpath = "//a[.='admin TWOFAEmail : admin2fa@etixlabs.com']\n")
    WebElement registeredUserSelect;
    @FindBy (xpath = "//*[@id=\"ui-select-choices-row-0-\"]/a")
    WebElement registeredUserSelectQA;
    @FindBy (xpath = "//input[@id='field_firstname']\n")
    WebElement guestName;
    @FindBy (xpath = "//input[@id='field_lastname']\n")
    WebElement guestLastName;
    @FindBy (xpath = "//input[@id='field_email']\n")
    WebElement guestMail;
    @FindBy (xpath = "//input[@id='field_company']\n")
    WebElement guestCompany;
    @FindBy (xpath = "//button[@id='addNewVisitorButton']\n")
    WebElement addVisitorButton;
    @FindBy (xpath = "//button[@class='btn btn-success btn-block']\n")
    WebElement endStepTwoButton;
    @FindBy (xpath = "//button[@class='btn btn-success btn-block']\n")
    WebElement endStepThreeButton;
    @FindBy (xpath = "//textarea[@name='purpose']\n")
    WebElement purposeInput;
    @FindBy (xpath = "//button[@class='btn btn-success btn-block']\n")
    WebElement endStepFourButton;
    @FindBy (xpath = "//*[@id=\"ar-page\"]/div[6]/div[7]/div/div/div[2]/button")
    WebElement validationButton;
    @FindBy (xpath = "//*[@id=\"content\"]/ui-view/div/div[2]/ul/li[1]/a")
    WebElement createdRequest;
    @FindBy (xpath = "//button[@class='btn btn-success']\n")
    WebElement validateRequest;
    @FindBy (xpath = "//button[.='Yes']\n")
    WebElement confirmValidation;
    @FindBy (xpath = "//button[@class='btn btn-danger']\n")
    WebElement denyValidation;
    @FindBy (xpath = "//button[@class='btn btn-warning']\n")
    WebElement revokeValidation;
    @FindBy (xpath = "//a[.='Edit']\n")
    WebElement editRequest;
    @FindBy (xpath = "//button[@class='btn btn-success btn-block']\n")
    WebElement endStepFour;
    @FindBy (xpath = "//input[@id='generalDate']\n")
    WebElement calendarOpener;
    @FindBy (xpath = "//tr[6]/td[.='29']\n")
    WebElement datePicker;
    @FindBy (xpath = "//*[@id=\"ar-page\"]/div[8]/div[8]/div/div/div[2]/button")
    WebElement editingValidation;
    @FindBy (xpath = "//*[@id=\"root\"]/div[1]/div[1]/div/div[2]/nav[3]/ul/div[14]/div/div/div/a[3]/div/h6")
    WebElement accessRequestsLogs;
    @FindBy (id = "accessRequestTypeFilter")
    WebElement typeDD;
    @FindBy (xpath = "//a[.='Rack intervention']")
    WebElement typeFirstItem;
    @FindBy (xpath = "//a[.='Commercial visit']")
    WebElement typeSecondItem;
    @FindBy (xpath = "//a[.='Device Maintenance']\n")
    WebElement typeThirdItem;
    @FindBy (xpath = "//a[.='Permanent access']\n")
    WebElement typeFourthItem;
    @FindBy (id = "accessRequestStatusFilter")
    WebElement statusDD;
    @FindBy (xpath = "//a[.='Waiting']")
    WebElement statusFirstItem;
    @FindBy (xpath = "//a[.='Validated']")
    WebElement statusSecondItem;
    @FindBy (xpath = "//a[.='Refused']\n")
    WebElement statusThirdItem;
    @FindBy (xpath = "//a[.='Revoked']\n")
    WebElement statusFourthItem;
    @FindBy (id = "accessRequestDatacenterFilter")
    WebElement dataCenterSelector;
    @FindBy (xpath = "//a[.='BelgiumDC']")
    WebElement dataCenterFirstItem;
    @FindBy (xpath = "//a[.='Etix Group Headquarters']")
    WebElement dataCenterSecondItem;
    @FindBy (xpath = "//a[.='Etix Lille #1']")
    WebElement dataCenterThirdItem;
    @FindBy (id = "accessRequestDateFilter")
    WebElement dateFilter;
    @FindBy (css = ".cancelBtn\n")
    WebElement noDateFilter;
    @FindBy (xpath = "//button[.='Reset filters']")
    WebElement resetFilters;
    @FindBy (css = "[placeholder='Search...']\n")
    WebElement searchField;
    @FindBy (css = ".item")
    WebElement foundItem;
    public void setAccessDD() {
        actionsWithWebElements.pushTheButton(accessDD);
    }
    public void setAccessRequest() {
        actionsWithWebElements.pushTheButton(accessRequest);
        wait_url.until(ExpectedConditions.urlMatches("https://beta.etixlabs.com/xlocker/accessrequests/"));
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
    }
    private void setNewRequest() {
        driver.switchTo().frame(driver.findElement(By.cssSelector("iframe[src*='/bms-webapp?lang=en&tz=local']\n")));
        wait_url.until(ExpectedConditions.elementToBeClickable(newRequest));
        actionsWithWebElements.pushTheButton(newRequest);
    }
    private void setSubjectInput(String subject) {
        actionsWithWebElements.inputTextToField(subjectInput, subject);
    }
    private void setDataCenter() throws InterruptedException {
        Select sel = new Select(dataCenterDD);
        sel.selectByIndex(1);
        Thread.sleep(2000);
    }
    private void setOrganization() throws InterruptedException {
        Select sel = new Select(organizationSelector);
        sel.selectByIndex(1);
        Thread.sleep(1000);
    }
    private void setType() throws InterruptedException {
        Select sel = new Select(typeSelector);
        sel.selectByIndex(1);
        Thread.sleep(1000);
    }
    private void clickTypeQA() {actionsWithWebElements.pushTheButton(typeSelectorQA);}
    private void setTypeQA() throws InterruptedException {
        Select sel = new Select(typeSelectorQA);
        sel.selectByIndex(1);
        Thread.sleep(1000);
    }
    public void setStepOne() {actionsWithWebElements.pushTheButton(endStepOneButton);}
    private void setRegisteredUserDD() {actionsWithWebElements.pushTheButton(registeredUserDD); }
    private void setRegisteredUserSelect() {actionsWithWebElements.pushTheButton(registeredUserSelect); }
    private void setRegisteredUserSelectQA() {actionsWithWebElements.pushTheButton(registeredUserSelectQA); }

    private void setGuestName(String name) {actionsWithWebElements.inputTextToField(guestName, name); }
    private void setGuestLastName(String lastName) {actionsWithWebElements.inputTextToField(guestLastName, lastName);}
    private void setGuestMail (String mail) {actionsWithWebElements.inputTextToField(guestMail, mail);}
    private void setGuestCompany(String company) { actionsWithWebElements.inputTextToField(guestCompany, company); }
    private void setAddVisitorButton() {actionsWithWebElements.pushTheButton(addVisitorButton);}
    public void setEndStepTwoButton() {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0,800);");
        wait_url.until(ExpectedConditions.elementToBeClickable(endStepTwoButton));
        actionsWithWebElements.pushTheButton(endStepTwoButton);}
    public void setStepThreeArea() {actionsWithWebElements.selectCheckBox("//*[@id=\"area152\"]", true);}
    public void setStepThreeAreaQA() {actionsWithWebElements.selectCheckBox("//*[@id=\"area148\"]", true);}
    public void setEndStepThreeButton() {actionsWithWebElements.pushTheButton(endStepThreeButton);}
    public void setPurposeInput(String purpose) {actionsWithWebElements.inputTextToField(purposeInput, purpose);}
    private void setEndStepFourButton() {actionsWithWebElements.pushTheButton(endStepFourButton);}
    private void setValidationButton()  {
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(500,8000);");
        actionsWithWebElements.pushTheButton(validationButton);}
    public void setCreatedRequest() {
        wait_url.until(ExpectedConditions.urlMatches("https://beta.etixlabs.com/xlocker/accessrequests/"));
        driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
//        driver.switchTo().frame(driver.findElement(By.cssSelector("iframe[src*='/bms-webapp?lang=en&tz=local']\n")));
        actionsWithWebElements.pushTheButton(createdRequest);
    }
    public void setValidateRequest() {actionsWithWebElements.pushTheButton(validateRequest); }
    public void setConfirmAction() {actionsWithWebElements.pushTheButton(confirmValidation);}
    public void createAccessRequest(String subject, String name, String lastName, String mail,String company, String purpose) throws InterruptedException {
        setAccessDD();
        setAccessRequest();
        setNewRequest();
        setSubjectInput(subject);
        setDataCenter();
        setOrganization();
        setType();
        setStepOne();
        setRegisteredUserDD();
        setRegisteredUserSelect();
        setGuestName(name);
        setGuestLastName(lastName);
        setGuestMail(mail);
        setGuestCompany(company);
        setAddVisitorButton();
        setEndStepTwoButton();
        setStepThreeArea();
        setEndStepThreeButton();
        setPurposeInput(purpose);
        setEndStepFourButton();
        setValidationButton();
        setCreatedRequest();
    }
    public void createAccessRequestQA(String subject, String name, String lastName, String mail,String company, String purpose) throws InterruptedException {
//        setAccessRequest();
        setNewRequest();
        setSubjectInput(subject);
//        setDataCenter();
//        setOrganization();
        Thread.sleep(1000);
        clickTypeQA();
        setTypeQA();
        setStepOne();
        setRegisteredUserDD();
        setRegisteredUserSelectQA();
        setGuestName(name);
        setGuestLastName(lastName);
        setGuestMail(mail);
        setGuestCompany(company);
        setAddVisitorButton();
        setEndStepTwoButton();
        setStepThreeAreaQA();
        setEndStepThreeButton();
        setPurposeInput(purpose);
        setEndStepFourButton();
        setValidationButton();
        setCreatedRequest();
    }
    public void setDenyValidation() {actionsWithWebElements.pushTheButton(denyValidation);}
    public void setRevokeValidation() {actionsWithWebElements.pushTheButton(revokeValidation);}
    public void setEditRequest() {
        wait_url.until(ExpectedConditions.elementToBeClickable(editRequest));
        actionsWithWebElements.pushTheButton(editRequest);}
    public void setEditType() throws InterruptedException {
//        wait_url.until(ExpectedConditions.elementToBeSelected(typeSelector));
        Select sel = new Select(typeSelector);
        sel.selectByIndex(2);
        Thread.sleep(1000);
    }
    public void setCalendarOpener() {actionsWithWebElements.pushTheButton(calendarOpener);}
    public void setDatePicker() {actionsWithWebElements.pushTheButton(datePicker);}
    public void setEndStepFour() {
        wait_url.until(ExpectedConditions.elementToBeClickable(endStepFour));
        actionsWithWebElements.pushTheButton(endStepFour);}
    public void setStepFiveActions() {actionsWithWebElements.selectCheckBox("//input[@name='cutting']\n", true);}
    public void setEditingValidation() {((JavascriptExecutor) driver).executeScript("window.scrollTo(0,18000);");
    actionsWithWebElements.pushTheButton(editingValidation); }
    public void setAccessRequestsLogs() {actionsWithWebElements.pushTheButton(accessRequestsLogs);}
    public void setTypeDD() {
        driver.switchTo().frame(driver.findElement(By.cssSelector("iframe[src*='/bms-webapp?lang=en&tz=local']\n")));
        wait_url.until(ExpectedConditions.elementToBeClickable(typeDD));
        actionsWithWebElements.pushTheButton(typeDD);}
    public void setTypeFirstItem() throws InterruptedException {actionsWithWebElements.pushTheButton(typeFirstItem);
        Thread.sleep(2000);}
    public void setTypeSecondItem() throws InterruptedException {
        actionsWithWebElements.pushTheButton(typeDD);
        actionsWithWebElements.pushTheButton(typeSecondItem);
        Thread.sleep(2000);}
    public void setTypeThirdItem() throws InterruptedException {
        actionsWithWebElements.pushTheButton(typeDD);
        actionsWithWebElements.pushTheButton(typeThirdItem);
        Thread.sleep(2000);}
    public void setTypeFourthItem() throws InterruptedException {
        actionsWithWebElements.pushTheButton(typeDD);
        actionsWithWebElements.pushTheButton(typeFourthItem);
        Thread.sleep(2000);}
    public void setStatusDD() {actionsWithWebElements.pushTheButton(statusDD);}
    public void setStatusFirstItem() throws InterruptedException {
        actionsWithWebElements.pushTheButton(statusFirstItem);
        Thread.sleep(2000); }
    public void setStatusSecondItem() throws InterruptedException {
        actionsWithWebElements.pushTheButton(statusDD);
        actionsWithWebElements.pushTheButton(statusSecondItem);
        Thread.sleep(2000); }
    public void setStatusThirdItem() throws InterruptedException {
        actionsWithWebElements.pushTheButton(statusDD);
        actionsWithWebElements.pushTheButton(statusThirdItem);
        Thread.sleep(2000); }
    public void setStatusFourthItem() throws InterruptedException {
        actionsWithWebElements.pushTheButton(statusDD);
        actionsWithWebElements.pushTheButton(statusFourthItem);
        Thread.sleep(2000); }
    public void setDataCenterSelector() {actionsWithWebElements.pushTheButton(dataCenterSelector);}
    public void setDataCenterFirstItem() throws InterruptedException {
        actionsWithWebElements.pushTheButton(dataCenterFirstItem);
        Thread.sleep(2000);
    }
    public void setDataCenterSecondItem() throws InterruptedException {
        actionsWithWebElements.pushTheButton(dataCenterSelector);
        actionsWithWebElements.pushTheButton(dataCenterSecondItem);
        Thread.sleep(2000);
    }
    public void setDataCenterThirdItem() throws InterruptedException {
        actionsWithWebElements.pushTheButton(dataCenterSelector);
        actionsWithWebElements.pushTheButton(dataCenterThirdItem);
        Thread.sleep(2000);
    }
    public void setDateFilter() {actionsWithWebElements.pushTheButton(dateFilter);}
    public void setNoDateFilter() throws InterruptedException {actionsWithWebElements.pushTheButton(noDateFilter);
    Thread.sleep(2000);}
    public void setResetFilters() {actionsWithWebElements.pushTheButton(resetFilters);}
    public void setSearchField(String number) throws InterruptedException {actionsWithWebElements.inputTextToField(searchField, number);
    driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        Thread.sleep(5000);
    }
    public void setFoundItem() throws InterruptedException {actionsWithWebElements.pushTheButton(foundItem);
    Thread.sleep(2000);}
    }



