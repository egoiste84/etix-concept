package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.xml.ws.FaultAction;

public class CctvPage extends ParentPage {
    private WebDriverWait wait_url = new WebDriverWait(driver, 75);

    public CctvPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//h6[.='CCTV']")
    WebElement cctvDD;
    @FindBy(xpath = "//*[@id=\"root\"]/div[1]/div[1]/div/div[2]/nav[2]/ul/div[6]/div/div/div/a[1]/div/h6")
    WebElement liveCamSelect;
    @FindBy(xpath = "//span[@class='MuiButton-label']\n")
    WebElement createGridButton;
    @FindBy(xpath = "//input[@id='name']\n")
    WebElement gridNameInput;
    @FindBy(xpath = "//input[@id='width']\n")
    WebElement gridWidthInput;
    @FindBy(xpath = "//input[@id='height']\n")
    WebElement gritHeightInput;
    @FindBy(xpath = "//span[.='Save']\n")
    WebElement gridSaveButton;
    @FindBy(xpath = "//span[.='Add Cameras']\n")
    WebElement addCamerasButton;
    @FindBy(xpath = "//*[@id=\"cctv-live-grid\"]/div/div[1]/ul/div[1]/li")
    WebElement source;
    @FindBy(xpath = "//*[@id=\"cctv-live-grid\"]/div/div[2]/div/div[2]")
    WebElement target;
    @FindBy(xpath = "//*[@id=\"react-select-3-option-3-0\"]")
    WebElement selectDataCenter;
    @FindBy(xpath = "//i[@name='more']\n")
    WebElement optionsIcon;
    @FindBy(xpath = "//span[.='Edit']\n")
    WebElement editOption;
    @FindBy (xpath = "//span[.='Clean Grid']\n")
    WebElement cleanGridOption;
    @FindBy (xpath = "//span[.='Confirm']\n")
    WebElement confirmCleanButton;
    @FindBy (xpath = "//input[@id='search']\n")
    WebElement dcSearchInput;
    @FindBy (xpath = "//span[.='Delete']\n")
    WebElement deleteOption;
    public void setCctvDD() {actionsWithWebElements.pushTheButton(cctvDD);}
    public void setLiveCamSelect() {actionsWithWebElements.pushTheButton(liveCamSelect);}
    public void setCreateGridButton() {
        actionsWithWebElements.pushTheButton(createGridButton);
        wait_url.until(ExpectedConditions.urlMatches("https://beta.etixlabs.com/cctv/grid/create"));}
    public void setGridNameInput(String name) {actionsWithWebElements.inputTextToField(gridNameInput, name);}
    public void setGridWidth(String width) {actionsWithWebElements.inputTextToField(gridWidthInput, width);}
    public void setGridHeight(String height) {actionsWithWebElements.inputTextToField(gritHeightInput, height);}
    public void setGridSave() {actionsWithWebElements.pushTheButton(gridSaveButton);}
    public void setAddCamerasButton() {actionsWithWebElements.pushTheButton(addCamerasButton);}
    public void DragAndDropJS() throws Exception {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("function createEvent(typeOfEvent) {\n" + "var event =document.createEvent(\"CustomEvent\");\n" + "event.initCustomEvent(typeOfEvent,true, true, null);\n" + "event.dataTransfer = {\n" + "data: {},\n" + "setData: function (key, value) {\n" + "this.data[key] = value;\n" + "},\n" + "getData: function (key) {\n" + "return this.data[key];\n" + "}\n" + "};\n" + "return event;\n" + "}\n" + "\n" + "function dispatchEvent(element, event,transferData) {\n" + "if (transferData !== undefined) {\n" + "event.dataTransfer = transferData;\n" + "}\n" + "if (element.dispatchEvent) {\n" + "element.dispatchEvent(event);\n" + "} else if (element.fireEvent) {\n" + "element.fireEvent(\"on\" + event.type, event);\n" + "}\n" + "}\n" + "\n" + "function simulateHTML5DragAndDrop(element, destination) {\n" + "var dragStartEvent =createEvent('dragstart');\n" + "dispatchEvent(element, dragStartEvent);\n" + "var dropEvent = createEvent('drop');\n" + "dispatchEvent(destination, dropEvent,dragStartEvent.dataTransfer);\n" + "var dragEndEvent = createEvent('dragend');\n" + "dispatchEvent(element, dragEndEvent,dropEvent.dataTransfer);\n" + "}\n" + "\n" + "var source = arguments[0];\n" + "var destination = arguments[1];\n" + "simulateHTML5DragAndDrop(source,destination);", source, target);
        Thread.sleep(1500);
    }
    public void setSelectDataCenter() {actionsWithWebElements.pushTheButton(selectDataCenter);}
    public void setOptionsIcon() {actionsWithWebElements.pushTheButton(optionsIcon);}
    public void setEditOption() {actionsWithWebElements.pushTheButton(editOption);}
    public void setGridNameInput2(String name) {
        gridNameInput.clear();
        actionsWithWebElements.inputTextToField(gridNameInput, name);}
    public void setCleanGridOption() {actionsWithWebElements.pushTheButton(cleanGridOption);}
    public void setConfirmCleanButton() {actionsWithWebElements.pushTheButton(confirmCleanButton);}
    public void setDcSearchInput(String dc) {
        dcSearchInput.clear();
        actionsWithWebElements.inputTextToField(dcSearchInput, dc);}
    public void setDeleteOption() {actionsWithWebElements.pushTheButton(deleteOption);}


}
