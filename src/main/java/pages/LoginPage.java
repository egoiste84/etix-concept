package pages;

import libs.ConfigData;
//import org.junit.Assert;
//import junit.framework.Assert;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;

import java.awt.*;
import java.util.concurrent.TimeUnit;

public class LoginPage extends ParentPage {
    WebDriverWait wait_url = new WebDriverWait(driver, 50);
    @BeforeTest
    public static void mouseMove(){
        try {
            // These coordinates are screen coordinates
            int xCoord = 0;
            int yCoord = 0;

            // Move the cursor
            Robot robot = new Robot();
            robot.mouseMove(xCoord, yCoord);
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }
    @FindBy(xpath = "//input[@id='email']")
    WebElement emailInput;

    @FindBy(xpath = "//input[@id='password']")
    WebElement passwordInput;

    @FindBy(css = ".MuiButtonBase-root\n")
    WebElement loginButton;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void OpenPageLogin() {
        driver.manage().window().maximize();

        try {
            driver.get(ConfigData.getCfgValue("base_url"));
            log.info("Login page is opened");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Can't open login page");
            Assert.fail("Can't reach URL");
        }
    }

    public void fillEmailField(String mail) {
        actionsWithWebElements.inputTextToField(emailInput, mail);
    }

    public void fillPasswordField(String pass) {
        actionsWithWebElements.inputTextToField(passwordInput, pass);
    }

    public void loginToBeta() throws InterruptedException {
        actionsWithWebElements.pushTheButton(loginButton);
        wait_url.until(ExpectedConditions.urlMatches("https://beta.etixlabs.com/xlocker/accessrequests/"));
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
        log.info("Successfully logged in to beta");
        Thread.sleep(3000);

    }

    public void loginUser(String mail, String pass) throws InterruptedException {
        mouseMove();
        OpenPageLogin();
        fillEmailField(mail);
        fillPasswordField(pass);
        loginToBeta();
    }
}
