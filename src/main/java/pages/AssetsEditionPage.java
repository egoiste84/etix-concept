package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AssetsEditionPage extends ParentPage{
    public AssetsEditionPage (WebDriver driver) {super (driver);}
    @FindBy (xpath = "//i[@name='more']\n")
    WebElement optionsIcon;
    @FindBy (xpath = "//span[.='Edit']")
    WebElement editOption;
	@FindBy(xpath="//*[@id=\"root\"]/div[1]/div[2]/div/div[2]/div[2]/div[2]/div/table/tbody/tr/td[9]/div/div/div[1]/div[2]/a[1]")
	WebElement editOption2;
	@FindBy (xpath = "//div[@class='Select css-5ojq3w has-value is-searchable Select--single']//i[@name='drop-arrow-bottom']\n")
    WebElement orientationDD;
    @FindBy (xpath = "//*[@id=\"react-select-3--option-2\"]")
    WebElement orientationSelect;
    @FindBy (xpath = "//span[3]/i[@name='drop-arrow-bottom']\n")
    WebElement positionDD;
    @FindBy (xpath = "//*[@id=\"react-select-4--option-2\"]")
    WebElement positionSelector;
    @FindBy (xpath = "/html/body/div[4]/div/div/div/div[2]/form/div[4]")
    WebElement editForm;
    @FindBy (css = ".Select-clear")
    WebElement clearPosition;
    @FindBy (xpath = "/html/body/div[4]/div/div/div/div[2]/form/div[4]/div/div/div/span[2]/i")
    WebElement newPositionDD;
    @FindBy (xpath = "//*[@id=\"react-select-4--option-1\"]")
    WebElement newPositionSelector;
    @FindBy (xpath = "//div[@class='CalendarMonthGrid CalendarMonthGrid--horizontal']//td[1]//div[.='29']\n")
    WebElement newEndDate;
    @FindBy(xpath = "/html/body/div[4]/div/div/div/div[2]/form/div[8]/button[3]/div/span")
    WebElement submitBookingButton;
    @FindBy (xpath = "//*[@id=\"root\"]/div[1]/div[2]/form/div/div[4]/div[2]/div/div/div/span[2]")
    WebElement newAssetPositionDD;
    @FindBy (xpath = "//div[@class='css-kvpiya']\n")
    WebElement assetEditForm;
    @FindBy (xpath = "//*[@id=\"react-select-6--option-0\"]")
    WebElement newAssetPosition;
    @FindBy (xpath = "//span[.='Delete']\n")
    WebElement deleteElement;
    @FindBy (xpath = "//*[@id=\"root\"]/div[1]/div[2]/div/div[2]/div[2]/div[2]/div/table/tbody/tr/td[9]/div/div/div[1]/div[2]/a[2]/span")
    WebElement deleteElement2;
    @FindBy (xpath = "//a[.='Delete']")
    WebElement deleteElement3;
    @FindBy (xpath = "//h6[.='LOGS']\n")
    WebElement activityLogs;
    @FindBy (xpath = "//*[@id=\"root\"]/div[1]/div[1]/div/div[2]/nav[3]/ul/div[14]/div/div/div/a[2]/div/h6")
    WebElement assetsLogs;
    @FindBy (xpath = "//input[@id='startDate']\n")
    WebElement calendarOpener;
    @FindBy (css = ".CalendarDay--today\n")
    WebElement startDateSelector;
    @FindBy (xpath = "//button[@class='css-17koqkg']")
    WebElement dateRangeApplyButton;
    @FindBy (xpath = "//span[.='Export']")
    WebElement exportButton;
    WebDriverWait wait_url = new WebDriverWait(driver, 60);
    public void setOptionsIcon() {((JavascriptExecutor) driver).executeScript("window.scrollTo(0,1600);");
        actionsWithWebElements.pushTheButton(optionsIcon);}
    public void setEditOption() {actionsWithWebElements.pushTheButton(editOption);}
    public void setEditOption2() {actionsWithWebElements.pushTheButton(editOption2);}

    public void setOrientationDD(){actionsWithWebElements.pushTheButton(orientationDD);}
    public void setOrientationSelect() {actionsWithWebElements.pushTheButton(orientationSelect);}
    public void setPositionDD() {actionsWithWebElements.pushTheButton(positionDD);}
    public void setPositionSelector() {actionsWithWebElements.pushTheButton(positionSelector);}
    public void checkWarning() {try {
        wait_url.until(ExpectedConditions.textToBePresentInElement(editForm, "This booking will overlap with installed asset"));
        log.info("Warning found");
    } catch (NoSuchElementException e) {
        log.error("No warning found");
    }}
    public void setClearPosition() throws InterruptedException {actionsWithWebElements.pushTheButton(clearPosition);
    Thread.sleep(1000);}

    public void setNewPositionDD() {actionsWithWebElements.pushTheButton(newPositionDD);}
    public void setNewPositionSelector() {actionsWithWebElements.pushTheButton(newPositionSelector);}
    public void setNewEndDate() {actionsWithWebElements.pushTheButton(newEndDate);}
    public void setNewAssetPositionDD() {actionsWithWebElements.pushTheButton(newAssetPositionDD);}
    public void setAssetEditForm() {try {
        wait_url.until(ExpectedConditions.textToBePresentInElement(assetEditForm, "This asset overlaps with a booked space. Creation of the asset will delete previously existing booking."));
        log.info("Warning found");
    } catch (NoSuchElementException e) {
        log.error("No warning found");
    }}
    public void submitBooking() {actionsWithWebElements.pushTheButton(submitBookingButton);}
    public void setNewAssetPosition() {actionsWithWebElements.pushTheButton(newAssetPosition);}
    public void setDeleteElement() {actionsWithWebElements.pushTheButton(deleteElement);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0,0);");
        log.info("Element deleted");
    }
    public void setDeleteElement2() {actionsWithWebElements.pushTheButton(deleteElement2);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0,0);");
        log.info("Element deleted");
    }
    public void setDeleteElement3() {actionsWithWebElements.pushTheButton(deleteElement3);
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0,0);");
        log.info("Element deleted");
    }
    public void setActivityLogs() {actionsWithWebElements.pushTheButton(activityLogs);
        log.info("Switched to Activity Logs");
    }
    public void setAssetsLogs() {actionsWithWebElements.pushTheButton(assetsLogs);}
    public void setCalendarOpener() {actionsWithWebElements.pushTheButton(calendarOpener);}
    public void setStartDateSelector() {actionsWithWebElements.pushTheButton(startDateSelector);}
    public void setDateRangeApplyButton() {actionsWithWebElements.pushTheButton(dateRangeApplyButton);}
    public void setExportButton() throws InterruptedException {actionsWithWebElements.pushTheButton(exportButton);
    Thread.sleep(1000);}
}
