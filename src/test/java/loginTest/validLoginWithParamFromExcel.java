//package loginTest;
//
//import libs.ConfigData;
//import org.junit.Test;
//import parentTest.ParentTest;
//
//import java.io.IOException;
//import java.util.Map;
//
//public class validLoginWithParamFromExcel extends ParentTest {
//    public validLoginWithParamFromExcel (String browser) {
//        super(browser);
//    }
//    @Test
//    public void validLogin() throws IOException {
//        Map dataFromExcelForValidLoginTest = excelDriver.getData(ConfigData.getCfgValue("DATA_FILE"), "validLogOn");
//        loginPage.OpenPageLogin();
//        loginPage.fillLoginField(dataFromExcelForValidLoginTest.get("login").toString());
//        loginPage.fillPasswordField(dataFromExcelForValidLoginTest.get("pass").toString());
//        loginPage.confirmLogin();
//        checkAcceptanceCriteria("Avatar isn't present", HomePage.isAvatarPresent(), true);
//        checkAcceptanceCriteria("Title isn't expected", HomePage.getTitle(), "Учет запчастей");
//    }
//}
