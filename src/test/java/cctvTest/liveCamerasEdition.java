package cctvTest;

import org.junit.Test;
import parentTest.ParentTest;

public class liveCamerasEdition extends ParentTest {
    public liveCamerasEdition (String browser) {super(browser);}
    @Test
    public void liveCamerasEdit() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        cctvPage.setCctvDD();
        cctvPage.setLiveCamSelect();
        assetsPage.dataCenterDD();
        assetsPage.selectDataCenter();
        assetsPage.dataCenterDD();
        Thread.sleep(1000);
        cctvPage.setSelectDataCenter();
        cctvPage.setOptionsIcon();
        cctvPage.setEditOption();
        cctvPage.setGridNameInput2("Test Grid edit");
        cctvPage.setGridSave();
    }
}
