package cctvTest;

import org.junit.Test;
import parentTest.ParentTest;

public class liveCamerasDeletion extends ParentTest {
    public liveCamerasDeletion (String browser) {super(browser);}
    @Test
    public void liveCamerasDelete() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        cctvPage.setCctvDD();
        cctvPage.setLiveCamSelect();
        assetsPage.dataCenterDD();
        assetsPage.selectDataCenter();
        assetsPage.dataCenterDD();
        Thread.sleep(1000);
        cctvPage.setSelectDataCenter();
        cctvPage.setOptionsIcon();
        cctvPage.setDeleteOption();
        Thread.sleep(4000);
    }
}
