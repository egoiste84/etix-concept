package cctvTest;

import org.junit.Test;
import parentTest.ParentTest;

public class cleanGridAddNew extends ParentTest {
    public cleanGridAddNew (String browser) {super(browser);}
    @Test
    public void cleanGrid() throws Exception {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        cctvPage.setCctvDD();
        cctvPage.setLiveCamSelect();
        assetsPage.dataCenterDD();
        assetsPage.selectDataCenter();
        assetsPage.dataCenterDD();
        Thread.sleep(1000);
        cctvPage.setSelectDataCenter();
        cctvPage.setOptionsIcon();
        cctvPage.setCleanGridOption();
        cctvPage.setConfirmCleanButton();
        Thread.sleep(1000);
        cctvPage.setAddCamerasButton();
        Thread.sleep(2000);
        cctvPage.DragAndDropJS();
        cctvPage.DragAndDropJS();
        cctvPage.setDcSearchInput("Lille");
        Thread.sleep(1000);
        cctvPage.DragAndDropJS();
        cctvPage.DragAndDropJS();
        cctvPage.setGridSave();
        Thread.sleep(2000);
    }
}
