package cctvTest;

import org.junit.Test;
import parentTest.ParentTest;

import java.util.concurrent.TimeUnit;

public class liveCamerasTest extends ParentTest {
    public liveCamerasTest (String browser) {super(browser);}
    @Test
    public void liveCameras() throws Exception {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        cctvPage.setCctvDD();
        cctvPage.setLiveCamSelect();
        assetsPage.dataCenterDD();
        assetsPage.selectDataCenter();
        Thread.sleep(3000);
        cctvPage.setCreateGridButton();
        cctvPage.setGridNameInput("Test Grid");
        cctvPage.setGridWidth("2");
        cctvPage.setGridHeight("2");
        cctvPage.setGridSave();
        Thread.sleep(2000);
        cctvPage.setAddCamerasButton();
        Thread.sleep(2000);
        cctvPage.DragAndDropJS();
        cctvPage.DragAndDropJS();
        cctvPage.DragAndDropJS();
        cctvPage.DragAndDropJS();
        cctvPage.setGridSave();
        Thread.sleep(2000);
    }
}