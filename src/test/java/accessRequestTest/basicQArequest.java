package accessRequestTest;

import org.junit.Test;
import parentTest.ParentTest;

public class basicQArequest extends ParentTest {
    public basicQArequest (String browser) {super(browser);}
    @Test
    public void basicQA() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("basicQA@etixlabs.com", "Etixlab$42");
        accessRequestsPage.createAccessRequestQA("Automation test", "Den", "Chik", "egoiste84@me.com","K&C", "Automation test Access request by basicQA user");
        Thread.sleep(5000);
    }
}

