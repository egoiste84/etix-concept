package accessRequestTest;

import org.junit.Test;
import parentTest.ParentTest;

public class accessRequestRevoking extends ParentTest {
    public accessRequestRevoking(String browser) {super(browser);}
    @Test
    public void accessRequestRevoke() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        accessRequestsPage.createAccessRequest("Automation test", "Den", "Chik", "egoiste84@me.com","K&C", "Automation test Access request");
        accessRequestsPage.setValidateRequest();
        accessRequestsPage.setConfirmAction();
        Thread.sleep(3000);
        accessRequestsPage.setRevokeValidation();
        accessRequestsPage.setConfirmAction();
        Thread.sleep(5000);
    }
}
