package accessRequestTest;

import org.junit.Test;
import parentTest.ParentTest;

public class accessRequestFilters extends ParentTest {
    public accessRequestFilters (String browser) {super(browser);}
    @Test
    public void checkFilters() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
//        accessRequestsPage.setAccessDD();
//        accessRequestsPage.setAccessRequest();
        accessRequestsPage.setTypeDD();
        accessRequestsPage.setTypeFirstItem();
        accessRequestsPage.setTypeSecondItem();
        accessRequestsPage.setTypeThirdItem();
        accessRequestsPage.setTypeFourthItem();
        accessRequestsPage.setStatusDD();
        accessRequestsPage.setStatusFirstItem();
        accessRequestsPage.setStatusSecondItem();
        accessRequestsPage.setStatusThirdItem();
        accessRequestsPage.setStatusFourthItem();
        accessRequestsPage.setDataCenterSelector();
        accessRequestsPage.setDataCenterFirstItem();
        accessRequestsPage.setDataCenterSecondItem();
        accessRequestsPage.setDataCenterThirdItem();
        accessRequestsPage.setDateFilter();
        accessRequestsPage.setNoDateFilter();
        accessRequestsPage.setResetFilters();
        accessRequestsPage.setSearchField("4135");
        accessRequestsPage.setFoundItem();
    }
}
