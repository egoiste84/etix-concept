package accessRequestTest;

import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
import parentTest.ParentTest;

public class accessRequestEditing extends ParentTest {
    public accessRequestEditing (String browser) {super(browser);}
    @Test
    public void accessRequestEdit() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
//        accessRequestsPage.setAccessDD();
//        accessRequestsPage.setAccessRequest();
//        accessRequestsPage.setCreatedRequest();
        accessRequestsPage.createAccessRequest("Automation test", "Den", "Chik", "egoiste84@me.com","K&C", "Automation test Access request");
        accessRequestsPage.setValidateRequest();
        accessRequestsPage.setConfirmAction();
        Thread.sleep(3000);
        accessRequestsPage.setEditRequest();
        Thread.sleep(2000);
        accessRequestsPage.setEditType();
        accessRequestsPage.setCalendarOpener();
        accessRequestsPage.setDatePicker();
        accessRequestsPage.setStepOne();
        Thread.sleep(1000);
        accessRequestsPage.setEndStepTwoButton();
        accessRequestsPage.setStepThreeArea();
        accessRequestsPage.setEndStepThreeButton();
        Thread.sleep(1000);
        accessRequestsPage.setEndStepFour();
        Thread.sleep(1000);
        accessRequestsPage.setStepFiveActions();
        accessRequestsPage.setEndStepFour();
        accessRequestsPage.setPurposeInput("Edition of Automation test Access request");
        accessRequestsPage.setEndStepFour();
        accessRequestsPage.setEditingValidation();
        accessRequestsPage.setCreatedRequest();
        accessRequestsPage.setValidateRequest();
        accessRequestsPage.setConfirmAction();
        Thread.sleep(5000);
    }
}
