package accessRequestTest;

import org.junit.Test;
import parentTest.ParentTest;

public class accessRequestsLogs extends ParentTest {
    public accessRequestsLogs (String browser) {super(browser);}
    @Test
    public void accessRequestLog() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        Thread.sleep(1000);
        assetsEditionPage.setActivityLogs();
        Thread.sleep(1000);
        accessRequestsPage.setAccessRequestsLogs();
        assetsEditionPage.setCalendarOpener();
        assetsEditionPage.setStartDateSelector();
        assetsEditionPage.setDateRangeApplyButton();
        assetsEditionPage.setExportButton();
        Thread.sleep(5000);
    }
}
