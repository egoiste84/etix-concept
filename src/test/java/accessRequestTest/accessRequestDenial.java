package accessRequestTest;

import org.junit.Test;
import parentTest.ParentTest;

public class accessRequestDenial extends ParentTest {
    public accessRequestDenial(String browser) {super(browser);}
    @Test
    public void accessRequestDeny() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        accessRequestsPage.createAccessRequest("Automation test", "Den", "Chik", "egoiste84@me.com","K&C", "Automation test Access request");
        accessRequestsPage.setDenyValidation();
        accessRequestsPage.setConfirmAction();
        Thread.sleep(5000);
    }
}
