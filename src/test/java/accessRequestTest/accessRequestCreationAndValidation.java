package accessRequestTest;

import org.junit.Test;
import parentTest.ParentTest;
public class accessRequestCreationAndValidation extends ParentTest{
    public accessRequestCreationAndValidation(String browser) {super(browser);}
    @Test
    public void accessRequest() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        accessRequestsPage.createAccessRequest("Automation test", "Den", "Chik", "egoiste84@me.com","K&C", "Automation test Access request");
        accessRequestsPage.setValidateRequest();
        accessRequestsPage.setConfirmAction();
        Thread.sleep(5000);
    }
}
