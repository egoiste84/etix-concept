package usersTest;
import org.junit.Test;
import parentTest.ParentTest;

public class userCreation extends ParentTest {
    public userCreation(String browser) {
        super(browser);
    }
    @Test
    public void createUser() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        usersPage.goToAdminPanel();
        usersPage.setCreateUserButton();
        usersPage.setFirstNameField("Den");
        usersPage.setLastNameField("Chik");
        loginPage.fillEmailField("egoiste84@me.com");
        usersPage.setPhoneNumberField("698068544");
        usersPage.setOrganizationDD();
        usersPage.setOrganizationSelector();
        assetsPage.assetSave();
    }
}

