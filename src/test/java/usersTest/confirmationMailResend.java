package usersTest;

import org.junit.Test;
import parentTest.ParentTest;

public class confirmationMailResend extends ParentTest {
    public confirmationMailResend(String browser) {super (browser);}
    @Test
    public void resendMail() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        usersPage.goToAdminPanel();
        usersPage.setSearchField("egoiste84");
        usersPage.setMoreButton();
        usersPage.setResendConfirmMail();
    }
}
