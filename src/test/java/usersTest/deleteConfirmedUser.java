package usersTest;

import org.junit.Test;
import parentTest.ParentTest;

public class deleteConfirmedUser extends ParentTest {
    public deleteConfirmedUser(String browser) {super (browser);}
    @Test
    public void deleteUser() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        usersPage.goToAdminPanel();
        usersPage.setSearchField("egoiste84");
        usersPage.setMoreButton();
        usersPage.setDeleteUser();
        usersPage.setDeleteConfirmation();
    }
}
