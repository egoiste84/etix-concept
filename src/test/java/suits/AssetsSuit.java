package suits;


//import loginTest.invalidLoginWithParams;

import assetsTest.*;
import loginTest.validLogin;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

//import loginTest.validLoginWithParamFromExcel;
//import testDB.testDataBase;
//
@RunWith(Suite.class)
@Suite.SuiteClasses({
//        validLogin.class,
        rackSpaceBooking.class,
        assetCreation.class,
        cableCreation.class,
        cableChainCreation.class,
        rackSpaceBookingEdition.class,
        assetEdition.class,
        rackSpaceDeletion.class,
        cableChainDeletion.class,
        cableDeletion.class,
        assetDeletion.class,
        gettinReport.class
//            validLoginWithParamFromExcel.class,
//            invalidLoginWithParams.class,
//            testDataBase.class
})
//
public class AssetsSuit {}
