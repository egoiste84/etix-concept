package suits;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import accessRequestTest.*;
@RunWith(Suite.class)
@Suite.SuiteClasses({
        accessRequestCreationAndValidation.class,
        accessRequestDenial.class,
        accessRequestRevoking.class,
        accessRequestEditing.class,
        basicQArequest.class,
        accessRequestFilters.class,
        accessRequestsLogs.class
})
public class AccessRequestsSuit {
}
