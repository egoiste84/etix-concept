package suits;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import usersTest.confirmationMailResend;
import usersTest.deleteConfirmedUser;
import usersTest.userCreation;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        userCreation.class,
        confirmationMailResend.class,
        deleteConfirmedUser.class
})
public class UsersSuit {}
