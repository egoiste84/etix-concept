package suits;

import cctvTest.cleanGridAddNew;
import cctvTest.liveCamerasDeletion;
import cctvTest.liveCamerasEdition;
import cctvTest.liveCamerasTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        liveCamerasTest.class,
        liveCamerasEdition.class,
        cleanGridAddNew.class,
        liveCamerasDeletion.class
})
public class CCTVSuit {}
