package assetsTest;

import org.junit.Test;
import org.openqa.selenium.By;
import parentTest.ParentTest;

public class rackSpaceBookingEdition extends ParentTest {
    public rackSpaceBookingEdition(String browser) {super(browser);}
    @Test
    public void rackBookingEdition() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        assetsPage.selectAsset();
        assetsPage.setRacksSwitcher();
        assetsPage.dataCenterDD();
        assetsPage.selectDataCenter();
        assetsPage.selectRack();
        assetsEditionPage.setOptionsIcon();
        assetsEditionPage.setEditOption2();
        assetsEditionPage.setOrientationDD();
        assetsEditionPage.setOrientationSelect();
        assetsEditionPage.setPositionDD();
        assetsEditionPage.setPositionSelector();
        assetsEditionPage.checkWarning();
        assetsEditionPage.setClearPosition();
        assetsEditionPage.setNewPositionDD();
        assetsEditionPage.setNewPositionSelector();
        assetsPage.endDatePickerOpen();
        assetsEditionPage.setNewEndDate();
        assetsPage.commentSet("this is an edition");
        assetsEditionPage.submitBooking();
    }
}
