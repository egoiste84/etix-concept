package assetsTest;

import org.junit.Test;
import parentTest.ParentTest;

public class cableChainDeletion extends ParentTest {
    public cableChainDeletion(String browser) {super (browser);}
    @Test
    public void cableChainDelete() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        assetsPage.selectAsset();
        assetsPage.switchToCableChain();
        assetsPage.dataCenterDD();
        assetsPage.selectDataCenter();
        assetsEditionPage.setOptionsIcon();
        assetsEditionPage.setDeleteElement();
    }
}
