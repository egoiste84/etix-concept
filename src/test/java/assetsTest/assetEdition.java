package assetsTest;

import org.junit.Test;
import parentTest.ParentTest;

public class assetEdition extends ParentTest {
    public assetEdition(String browser) {super (browser);}
    @Test
    public void assetEdit() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        assetsPage.selectAsset();
        assetsPage.switchToAssets();
        assetsPage.dataCenterDD();
        assetsPage.selectDataCenter();
        assetsEditionPage.setOptionsIcon();
        assetsEditionPage.setEditOption();
        assetsEditionPage.setNewAssetPositionDD();
        assetsPage.setDestinationAssetTypeSelect();
        assetsEditionPage.setAssetEditForm();
        assetsEditionPage.setNewAssetPositionDD();
        assetsEditionPage.setNewAssetPosition();
        assetsPage.assetSave();
    }
}
