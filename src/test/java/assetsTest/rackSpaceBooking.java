package assetsTest;

import org.junit.Test;
import org.openqa.selenium.By;
import parentTest.ParentTest;

public class rackSpaceBooking extends ParentTest {
    public rackSpaceBooking(String browser) {super(browser);}

    @Test
    public void rackBooking() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        assetsPage.selectAsset();
        assetsPage.setRacksSwitcher();
        assetsPage.dataCenterDD();
        assetsPage.selectDataCenter();
        assetsPage.selectRack();
        assetsPage.addBook();
        assetsPage.selectDD();
        assetsPage.orientationSelect();
        assetsPage.positioningDD();
        assetsPage.positioningSelect(By.xpath("//*[@id=\"react-select-4--option-0\"]"));
        assetsPage.endDatePickerOpen();
        assetsPage.endDateSelect();
        assetsPage.commentSet("this is a test");
        assetsPage.submitBooking();
    }
}
