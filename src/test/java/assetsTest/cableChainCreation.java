package assetsTest;

import org.junit.Test;
import parentTest.ParentTest;

public class cableChainCreation extends ParentTest {
    public cableChainCreation(String browser) {super (browser);}
    @Test
    public void cableChain() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        assetsPage.selectAsset();
        assetsPage.switchToCableChain();
        assetsPage.dataCenterDD();
        assetsPage.selectDataCenter();
        assetsPage.createCableChain();
        assetsPage.fillCableChainName("testCableChain1");
        assetsPage.rackCompartmentDDOpen();
        assetsPage.rackCompartmentSelect();
        assetsPage.setAsset();
        assetsPage.assetSelectorDDOpen();
        assetsPage.assetSelect2();
        assetsPage.outOutletDDOpen();
        assetsPage.outOutletSelect();
        assetsPage.inRackCompartmentDDOpen();
        assetsPage.inRackCompartmentSelect();
        assetsPage.setInAsset();
        assetsPage.inAssetPduDDOpen();
        assetsPage.inAssetPduSelect();
        assetsPage.inOutletSelectDDOpen();
        assetsPage.inOutletSelect();
        assetsPage.createCable2();
        assetsPage.fillCableNameInput("testcable2");
        assetsPage.setCableType();
        assetsPage.setCableSaveButton();
        Thread.sleep(2000);
        assetsPage.setCableChainSaveButton();
    }
}
