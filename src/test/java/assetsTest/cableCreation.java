package assetsTest;

import org.junit.Test;
import parentTest.ParentTest;

public class cableCreation extends ParentTest {
    public cableCreation(String browser) {super(browser);}
    @Test
    public void cableCreate() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        assetsPage.selectAsset();
        assetsPage.switchToCables();
        assetsPage.dataCenterDD();
        assetsPage.selectDataCenter();
        assetsPage.createNewCable();
        assetsPage.fillCableName("testCable1");
        assetsPage.fillCableReference("testCable1");
        assetsPage.cableTypeDDOpen();
        assetsPage.cableTypeSelect();
        assetsPage.sourceAssetTypeDDOpen();
        assetsPage.sourceAssetTypeSelect();
        assetsPage.sourceAssetDDOpen();
        assetsPage.sourceAssetSelect();
        assetsPage.sourceOutletDDOpen();
        assetsPage.sourceOutletSelect();
        assetsPage.destinationAssetTypeDDOpen();
        assetsPage.setDestinationAssetTypeSelect();
        assetsPage.destinationAssetDDOpen();
        assetsPage.destinationAssetSelect();
        assetsPage.destinationOutletDDOpen();
        assetsPage.destinationOutletSelect();
        assetsPage.createCable();
    }
}
