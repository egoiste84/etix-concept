package assetsTest;

import org.junit.Test;
import parentTest.ParentTest;

public class cableDeletion extends ParentTest {
    public cableDeletion (String browser) {super(browser);}
    @Test
    public void cableDelete() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        assetsPage.selectAsset();
        assetsPage.switchToCables();
        assetsPage.dataCenterDD();
        assetsPage.selectDataCenter();
        assetsEditionPage.setOptionsIcon();
        assetsEditionPage.setDeleteElement();
        Thread.sleep(1000);
        driver.navigate().refresh();
        assetsEditionPage.setOptionsIcon();
        assetsEditionPage.setDeleteElement();
    }
}
