package assetsTest;

import org.junit.Test;
import parentTest.ParentTest;

public class assetCreation extends ParentTest {
    public assetCreation(String browser) {super(browser);}
    @Test
    public void assetCreate() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        assetsPage.selectAsset();
        assetsPage.switchToAssets();
        assetsPage.dataCenterDD();
        assetsPage.selectDataCenter();
        assetsPage.createNew();
        assetsPage.rackDDSelection();
        assetsPage.rackSelection();
        assetsPage.assetModelDDSelection();
        assetsPage.assetModelSelection();
        assetsPage.assetPositionDDOpen();
        assetsPage.assetPositionSelect();
        assetsPage.assetDirectionDDOpen();
        assetsPage.assetDirectionSelect();
        assetsPage.fillNameField("DenTest2");
        assetsPage.fillReferenceField("dentest2");
        assetsPage.setRadioInstalled();
        assetsPage.setPowerOutlet2();
        assetsPage.setNetworkOutlet1();
        assetsPage.setNetworkOutlet3();
        assetsPage.setConsoleOutlet2();
        assetsPage.setConsoleOutlet4();
        assetsPage.assetSave();
    }
}
