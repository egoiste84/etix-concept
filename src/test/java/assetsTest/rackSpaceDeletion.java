package assetsTest;

import org.junit.Test;
import parentTest.ParentTest;

public class rackSpaceDeletion extends ParentTest {
    public rackSpaceDeletion(String browser) {super (browser);}
    @Test
    public void rackSpaceDelete() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        assetsPage.selectAsset();
        assetsPage.setRacksSwitcher();
        assetsPage.dataCenterDD();
        assetsPage.selectDataCenter();
        assetsPage.selectRack();
        assetsEditionPage.setOptionsIcon();
        assetsEditionPage.setDeleteElement2();
    }
}
