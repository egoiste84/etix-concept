package assetsTest;

import org.junit.Test;
import parentTest.ParentTest;

public class gettinReport extends ParentTest {
    public gettinReport (String browser) {super(browser);}
    @Test
    public void getReport() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        assetsEditionPage.setActivityLogs();
        Thread.sleep(1000);
        assetsEditionPage.setAssetsLogs();
        assetsPage.dataCenterDD();
        assetsPage.selectDataCenter();
        assetsEditionPage.setCalendarOpener();
        assetsEditionPage.setStartDateSelector();
        assetsEditionPage.setDateRangeApplyButton();
        assetsEditionPage.setExportButton();
        Thread.sleep(5000);
    }
}
