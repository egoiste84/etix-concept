package assetsTest;

import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
import parentTest.ParentTest;

public class assetDeletion extends ParentTest {
    public assetDeletion (String browser) {super (browser);}
    @Test
    public void assetDelete() throws InterruptedException {
        loginPage.OpenPageLogin();
        loginPage.loginUser("admin2FA@etixlabs.com", "Etixlab$42");
        assetsPage.selectAsset();
        assetsPage.switchToAssets();
        assetsPage.dataCenterDD();
        assetsPage.selectDataCenter();
        assetsEditionPage.setOptionsIcon();
        assetsEditionPage.setDeleteElement3();
        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0,0);");
    }
}
